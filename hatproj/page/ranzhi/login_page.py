from base import Yaml, Path
from page import Page


class LoginPage(Page):
    __config = Yaml.get_config_as_dict(Path.get_actual_path(__file__, "../../page/ranzhi/ranzhi.yaml"))["LoginPage"]

    def input_login_info(self, account, password):
        """
        输入用户名，密码
        :return:
        """
        self.driver.type(self.__config["LOGIN_FORM_ACCOUNT_SELECTOR"], account)
        self.driver.type(self.__config["LOGIN_FORM_PASSWORD_SELECTOR"], password)
        self.driver.forced_wait(2)

    def do_login(self):
        """
        点击登录按钮
        :return:
        """
        self.driver.click(self.__config["LOGIN_FORM_SUBMIT_SELECTOR"])
        self.driver.forced_wait(2)

    # 方法

    def login_and_submit(self, account, password, keep):
        """
        登录系统
        :param account:
        :param password:
        :param keep:
        :return: 返回保持登录复选框的 checked 值
        """
        self.driver.type(self.__config["LOGIN_FORM_ACCOUNT_SELECTOR"], account)
        self.driver.type(self.__config["LOGIN_FORM_PASSWORD_SELECTOR"], password)

        current_checked = self.current_keep_value
        if keep:
            if current_checked is None:
                self.driver.click(self.__config["LOGIN_FORM_KEEP_SELECTOR"])
        else:
            if current_checked == "true":
                self.driver.click(self.__config["LOGIN_FORM_KEEP_SELECTOR"])

        actual_checked = self.current_keep_value
        self.driver.click(self.__config["LOGIN_FORM_SUBMIT_SELECTOR"])
        self.driver.forced_wait(2)
        return actual_checked

    @property
    def current_keep_value(self):
        """
        获取当前的 保持登录 复选框 的值
        :return:
        """
        return self.driver.get_attribute(self.__config["LOGIN_FORM_KEEP_SELECTOR"], "checked")

        # def change_language(self, lang):

    def change_language(self, lang):
        """
        更改 登录页面语言
        :param lang: en, zh_CN, zh_TW
        :return:
        """
        self.driver.click(self.__config["LOGIN_LANGUAGE_BUTTON_SELECTOR"])
        self.driver.forced_wait(1)

        if lang == "en":
            lang_number = 3
        elif lang == "zh_CN":
            lang_number = 1
        elif lang == "zh_TW":
            lang_number = 2
        else:
            lang_number = 0

        self.driver.click(self.__config["LOGIN_LANGUAGE_MENU_SELECTOR"] % lang_number)
        self.driver.forced_wait(2)

    @property
    def text_dict_for_language(self):
        """
        获取当前页面的文字，语言按钮和登录按钮，以字典类型返回
        :return: 字典
        """

        lang_button_text = self.driver.get_text(self.__config["LOGIN_LANGUAGE_BUTTON_SELECTOR"])
        login_button_text = self.driver.get_text(self.__config["LOGIN_FORM_SUBMIT_SELECTOR"])

        text_dict = {
            "lang": lang_button_text,
            "login": login_button_text
        }
        return text_dict

    @property
    def fail_login_message(self):
        return self.driver.get_text(self.__config["LOGIN_FAIL_MESSAGE_SELECTOR"])

    def login_with_cookie(self, rid_value, url, lang_value='zh-cn'):
        """
        使用 cookie 登录 然之系统
        :param rid_value: cookie rid 的value
        :param url: 登录地址 url
        :param lang_value: cookie lang 的value，默认值是 zh-cn
        :return: 无
        """
        # 删除 cookie rid
        self.driver.remove_cookie(self.__config["COOKIE_RID_NAME"])
        # 添加新的 cookie rid
        cookie_rid_dict = {"name": self.__config["COOKIE_RID_NAME"],
                           "value": rid_value}
        self.driver.add_cookie(cookie_rid_dict)

        # 删除 cookie lang
        self.driver.remove_cookie(self.__config["COOKIE_LANG_NAME"])
        # 添加新的 cookie lang
        cookie_lang_dict = {"name": self.__config["COOKIE_LANG_NAME"],
                            "value": lang_value}
        self.driver.add_cookie(cookie_lang_dict)

        # 刷新浏览器
        self.driver.refresh(url)
        # 等待3秒钟
        self.driver.forced_wait(3)
