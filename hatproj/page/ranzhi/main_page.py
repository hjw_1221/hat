from time import sleep

from base import Yaml, Path
from page.ranzhi.login_page import LoginPage


class MainPage(LoginPage):
    __config = Yaml().get_config_as_dict(Path.get_actual_path(__file__, "../../page/ranzhi/ranzhi.yaml"))["MainPage"]

    def do_logout_by_lang(self, lang):
        """
        退出
        :return:
        """
        self.driver.click(self.__config["LOGOUT_AVATAR_SELECTOR"])
        sleep(1)
        if lang == "en":
            self.driver.click(self.__config["LOGOUT_MENU_EN_TEXT"])
        else:
            self.driver.click(self.__config["LOGOUT_MENU_ZH_TEXT"])

        sleep(2)

    def select_app(self, app):
        """
        选择 应用
        :param app: oa，crm，cash，team，proj，admin，doc
        :return:
        """
        self.log("[MainPage] 开始 select_app(%s)" % app)
        if app == "oa":
            self.driver.click(self.__config["LEFT_BAR_APP_OA_SELECTOR"])
        elif app == "crm":
            self.driver.click(self.__config["LEFT_BAR_APP_CRM_SELECTOR"])
        elif app == "cash":
            self.driver.click(self.__config["LEFT_BAR_APP_CASH_SELECTOR"])
        elif app == "team":
            self.driver.click(self.__config["LEFT_BAR_APP_TEAM_SELECTOR"])
        elif app == "proj":
            self.driver.click(self.__config["LEFT_BAR_APP_PROJECT_SELECTOR"])
        elif app == "admin":
            self.driver.click(self.__config["LEFT_BAR_APP_ADMIN_SELECTOR"])
        elif app == "doc":
            self.driver.click(self.__config["LEFT_BAR_APP_DOC_SELECTOR"])
        self.log("[MainPage] 结束 select_app(%s)" % app)
        sleep(2)

    @property
    def navbar_real_name(self):
        """
        获取系统在顶层主菜单的真实姓名
        :return:
        """
        return self.driver.get_text(self.__config["MAIN_BAR_ACCOUNT_SELECTOR"])

    @property
    def start_real_name(self):
        """
        获取系统在退出登录菜单的真实姓名
        :return:
        """
        self.driver.click(self.__config["LOGOUT_AVATAR_SELECTOR"])
        sleep(1)
        real_name = self.driver.get_text(self.__config["LOGOUT_MENU_ACCOUNT_SELECTOR"])

        self.driver.click(self.__config["LOGOUT_AVATAR_SELECTOR"])
        sleep(1)
        return real_name

    @property
    def real_name_by_click_main_bar(self):
        """
        点击左上角的真实姓名标签，获取真实姓名
        :return:
        """
        self.driver.click(self.__config["MAIN_BAR_ACCOUNT_SELECTOR"])
        sleep(2)
        real_name = self.driver.get_text(self.__config["MAIN_BAR_ACCOUNT_DIALOG_REAL_NAME_SELECTOR"])
        sleep(1)
        self.driver.click(self.__config["MAIN_BAR_ACCOUNT_DIALOG_CLOSE_SELECTOR"])
        sleep(1)
        return real_name

    def select_menu(self, menu):
        """
        在 系统登录后进入的主页 直接点击进入菜单
        注意：此时未进入任何应用，包括我的地盘
        :param menu: todo, task, project, order, contract, review, company, dynamic
        :return:
        """
        menu_selector = self._get_main_menu_selector(menu)

        self.driver.click(menu_selector)
        sleep(2)

    def _get_main_menu_selector(self, menu):
        """
        获取指定 menu 的 Selector
        :param menu: todo, task, project, order, contract, review, company, dynamic
        :return:
        """

        if menu == "todo":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 1
        elif menu == "task":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 2
        elif menu == "project":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 3
        elif menu == "order":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 4
        elif menu == "contract":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 5
        elif menu == "review":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 6
        elif menu == "company":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 7
        elif menu == "dynamic":
            menu_selector = self.__config["MAIN_BAR_MENU_SELECTOR"] % 8
        else:
            menu_selector = "error menu"

        return menu_selector

    @property
    def language_from_start_menu(self):
        """
        获取主页开始菜单中的 当前语言
        :return: 当前语言 str
        """
        self.driver.click(self.__config["LOGOUT_AVATAR_SELECTOR"])
        sleep(1)
        lang = self.driver.get_text(self.__config["LOGOUT_MENU_LANG_SELECTOR"])
        self.driver.click(self.__config["LOGOUT_AVATAR_SELECTOR"])
        sleep(1)
        return lang

    def click_menu(self, menu):
        """
        点击主页导航的 菜单
        :param menu:
            待办：todo_calendar
            任务：task
            项目：project
            订单：order
            合同：contract
            审批：review
            组织：company
            动态：dynamic
        :return:
        """
        menu_selector = self._get_menu_selector(menu)

        if menu_selector is not None:
            self.driver.click(menu_selector)
            self.driver.forced_wait(5)

    def do_logout(self, is_admin=False):
        """
        退出登录
        :return:
        """
        self.driver.click(self.__config["START_BUTTON_SELECTOR"])
        self.driver.forced_wait(1)

        if is_admin:
            self.driver.click(self.__config["START_EXIT_MENU_SELECTOR"] % 10)
        else:
            self.driver.click(self.__config["START_EXIT_MENU_SELECTOR"] % 8)

    def _get_menu_selector(self, menu):
        """
        根据菜单的名字获取菜单的定位符
        :param menu:
            待办：todo_calendar
            任务：task
            项目：project
            订单：order
            合同：contract
            审批：review
            组织：company
            动态：dynamic
        :return:
        """
        menu_selector = None

        if menu == "todo_calendar":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 1

        elif menu == "task":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 2

        elif menu == "project":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 3

        elif menu == "order":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 4

        elif menu == "contract":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 5

        elif menu == "review":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 6

        elif menu == "company":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 7

        elif menu == "dynamic":
            menu_selector = self.__config["NAV_MENU_SELECTOR"] % 8

        return menu_selector
