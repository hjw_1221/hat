from base import Yaml, Path, build_request, Json
from page.nsp.api.user_api import UserApi


class CartApi(UserApi):
    __config = Yaml.get_config_as_dict(Path.get_actual_path(__file__, "../nsp.yaml"))["CartApi"]

    def get_current(self, sign=None, token=None):

        sign, token = self._get_member_auth(sign, token)
        data = {
            "sign": sign
        }
        api_name = self.__config["API_NAME_GET_CURRENT"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data,
            auth=token
        )
        self._response = req.response

        res = self._parse_cart_goods(req.json_dict)

        return res

    def get_list(self, uid, sign=None, token=None):
        """
        get list by uid, admin
        :param sign:
        :param token:
        :param uid:
        :return:
        """
        sign, token = self._get_admin_auth(sign, token)

        data = {
            "sign": sign,
            "uid": uid
        }

        api_name = self.__config["API_NAME_GET_List"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data,
            auth=token
        )
        self._response = req.response

        res = self._parse_cart_goods(req.json_dict)

        return res

    def _parse_cart_goods(self, json_dict):
        """
        parse cart goods to dict
        :param json_dict
        :return:
        """
        resp_dict = self._parse_resp(json_dict)

        if resp_dict["data"]:
            resp_dict["code"], resp_dict["msg"] = self._parse_data_code_and_msg(json_dict)

        return resp_dict


