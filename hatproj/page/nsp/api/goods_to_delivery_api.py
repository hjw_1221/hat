from base import Yaml, Path, build_request, Json
from page.nsp.api.order_api import OrderApi


class GoodsToDeliveryApi(OrderApi):
    __config = Yaml.get_config_as_dict(Path.get_actual_path(__file__, "../nsp.yaml"))["GoodsToDeliveryApi"]

    def get(self, order_id, sign=None, token=None):
        """
        get request
        :param order_id:
        :param sign:
        :param token:
        :return:
            dict:
            std:
            order_goods_id_list: str, "1" or "1,2,3"
        """

        sign, token = self._get_admin_auth(sign, token)

        data = {
            "sign": sign,
            "orderId": order_id
        }

        api_name = self.__config["API_NAME_GET"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data,
            auth=token
        )
        self._response = req.response

        return self._parse_goods_to_delivery(req.json_dict)

    def _parse_goods_to_delivery(self, json_dict):
        """
        _parse_goods_to_delivery
        :param json_dict:
        :return:
        """

        resp_dict = self._parse_resp(json_dict)

        if resp_dict["data"]:
            resp_dict["data_code"], resp_dict["data_msg"] = self._parse_data_code_and_msg(json_dict)

            resp_dict["order_goods_id_list"] = self._parse_order_goods_id_list(json_dict)

        return resp_dict

    def _parse_order_goods_id_list(self, json_dict):
        """
        _parse_order_goods_id_list
        :param json_dict:
        :return:
        """
        result_list = Json.parse_json_dict_value(json_dict,
                                                 data_key=self.__config["DATA_RESULT"])

        order_id_list = []
        for result in result_list:
            order_id_list.append(Json.parse_json_dict_value(result,
                                                            data_key=self.__config["ORDER_GOODS_ID"]))
        return ",".join(order_id_list)
