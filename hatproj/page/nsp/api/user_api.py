from base import build_request, Yaml, Path, Json
from page import Api


class UserApi(Api):
    """
    User API, Admin Login and  User Login
    """
    __config = Yaml.get_config_as_dict(Path.get_actual_path(__file__, "../nsp.yaml"))["UserApi"]

    _member_auth = ()
    _admin_auth = ()

    def login_member(self, account, password):
        """
        login member
        :param account:
        :param password:
        :return: dict:
            token:
            resp_data:
            uid:
            is_login:
            admin_status:
            ret:
        """
        data = {
            "account": account,
            "password": password,
            "sign": account
        }

        api_name = self.__config["API_NAME_LOGIN_MEMBER"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data
        )
        self._response = req.response

        resp = self._parse_member_resp(req.json_dict)

        self._member_auth = (account, resp["token"])
        return resp

    def _parse_member_resp(self, json_dict):
        """
        parse member resp
        :param json_dict:
        :return:
        """
        resp_dict = self._parse_resp(json_dict)

        if resp_dict["data"]:
            resp_dict["data_code"], resp_dict["data_msg"] = self._parse_data_code_and_msg(json_dict)

            resp_dict["token"] = self._parse_result_list(json_dict,
                                                         self.__config["MEMBER_DATA_RESULT_INDEX"],
                                                         self.__config["MEMBER_TOKEN"])

            resp_dict["uid"] = self._parse_result_list(json_dict,
                                                       self.__config["MEMBER_DATA_RESULT_INDEX"],
                                                       self.__config["MEMBER_UID"])

            resp_dict["is_login"] = self._parse_result_list(json_dict,
                                                            self.__config["MEMBER_DATA_RESULT_INDEX"],
                                                            self.__config["MEMBER_IS_LOGIN"])

        return resp_dict

    def login_admin(self, account, password):
        """
        login admin
        :param account:
        :param password:
        :return:
        """
        data = {
            "account": account,
            "password": password,
            "sign": account
        }

        api_name = self.__config["API_NAME_LOGIN_ADMIN"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data
        )
        self._response = req.response

        resp = self._parse_admin_resp(req.json_dict)

        self._admin_auth = (account, resp["token"])
        return resp

    def _parse_admin_resp(self, json_dict):
        """
        parse admin resp
        :param json_dict:
        :return:
        """
        resp_dict = self._parse_resp(json_dict)

        if resp_dict["data"]:
            resp_dict["data_code"], resp_dict["data_msg"] = self._parse_data_code_and_msg(json_dict)

            resp_dict["token"] = self._parse_result_list(json_dict,
                                                         self.__config["ADMIN_DATA_RESULT_INDEX"],
                                                         self.__config["ADMIN_TOKEN"])

            resp_dict["uid"] = self._parse_result_list(json_dict,
                                                       self.__config["ADMIN_DATA_RESULT_INDEX"],
                                                       self.__config["ADMIN_UID"])

            resp_dict["is_login"] = self._parse_result_list(json_dict,
                                                            self.__config["ADMIN_DATA_RESULT_INDEX"],
                                                            self.__config["ADMIN_IS_LOGIN"])

            resp_dict["admin_status"] = self._parse_result_list(json_dict,
                                                                self.__config["ADMIN_DATA_RESULT_INDEX"],
                                                                self.__config["ADMIN_STATUS"])

        return resp_dict

    """
    base methods
    """

    def _parse_resp(self, json_dict):
        """
        parse resp
        :param json_dict:
        :return: dict
            ret:
            msg:
            data:
        """
        ret = Json.parse_json_dict_value(json_dict, data_key=self.__config["API_REP_BASE_RET"])
        msg = Json.parse_json_dict_value(json_dict, data_key=self.__config["API_REP_BASE_MSG"])
        data = Json.parse_json_dict_value(json_dict, data_key=self.__config["API_REP_BASE_DATA"])

        return {
            "ret": ret,
            "msg": msg,
            "data": data
        }

    def _parse_result_list(self, json_dict, index, sub_key):
        """
        parse result list
        :param json_dict:
        :param index:
        :param sub_key:
        :return:
        """
        return Json.parse_json_dict_value(json_dict,
                                          data_key=self.__config["API_REP_BASE_DATA_RESULT"],
                                          index=index,
                                          sub_key=sub_key)

    def _parse_data_code_and_msg(self, json_dict):
        """
        parse data code and msg
        :param json_dict:
        :return: code, msg
        """
        code = Json.parse_json_dict_value(json_dict,
                                          data_key=self.__config["API_REP_BASE_DATA_CODE"])
        msg = Json.parse_json_dict_value(json_dict,
                                         data_key=self.__config["API_REP_BASE_DATA_MSG"])
        return code, msg

    def _get_member_auth(self, sign, token):
        """
        get auth
        :param sign:
        :param token:
        :return:
        """
        if not sign:
            sign = self._member_auth[0]

        if not token:
            token = self._member_auth[1]

        return sign, token

    def _get_admin_auth(self, sign, token):
        """
        get auth
        :param sign:
        :param token:
        :return:
        """
        if not sign:
            sign = self._admin_auth[0]

        if not token:
            token = self._admin_auth[1]

        return sign, token

    def _parse_standard_resp(self, json_dict):
        """
        std resp
        :param json_dict:
        :return:
        """
        resp_dict = self._parse_resp(json_dict)

        if resp_dict["data"]:
            resp_dict["data_code"], resp_dict["data_msg"] = self._parse_data_code_and_msg(json_dict)

        return resp_dict
