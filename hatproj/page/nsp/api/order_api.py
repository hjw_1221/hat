from base import Yaml, Path, build_request
from page.nsp.api.user_api import UserApi


class OrderApi(UserApi):
    __config = Yaml.get_config_as_dict(Path.get_actual_path(__file__, "../nsp.yaml"))["OrderApi"]

    def get_by_no(self, order_no, sign=None, token=None):
        """
        get by no
        :param order_no:
        :param sign:
        :param token:
        :return:
            dict:
                std
                order_id: int
        """
        sign, token = self._get_admin_auth(sign, token)

        data = {
            "sign": sign,
            "order_no": order_no
        }

        api_name = self.__config["API_NAME_GET_BY_NO"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data,
            auth=token
        )
        self._response = req.response

        return self._parse_order(req.json_dict)

    def _parse_order(self, json_dict):
        """
        parse order
        :param json_dict: 
        :return: 
        """

        resp_dict = self._parse_resp(json_dict)

        if resp_dict["data"]:
            resp_dict["code"], resp_dict["msg"] = self._parse_data_code_and_msg(json_dict)

            resp_dict["order_id"] = self._parse_result_list(json_dict,
                                                            self.__config["ORDER_DATA_RESULT_INDEX"],
                                                            self.__config["ORDER_ID"])

        return resp_dict
