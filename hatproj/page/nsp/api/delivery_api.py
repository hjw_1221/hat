from base import Yaml, Path, build_request, Json
from page.nsp.api.goods_to_delivery_api import GoodsToDeliveryApi


class DeliveryApi(GoodsToDeliveryApi):
    __config = Yaml.get_config_as_dict(Path.get_actual_path(__file__, "../nsp.yaml"))["DeliveryApi"]

    def add(self,
            order_id,
            express_type,
            express_info,
            order_goods_id_array,
            sign=None,
            token=None):
        """
        发货
        :param order_id:
        :param express_type:
        :param express_info:
        :param order_goods_id_array:
        :param sign:
        :param token:
        :return:
        """

        sign, token = self._get_admin_auth(sign, token)

        data = {
            "sign": sign,
            "id": order_id,
            "expressType": express_type,
            "expressInfo": express_info,
            "orderGoodsIdArray": order_goods_id_array
        }

        api_name = self.__config["API_NAME_ADD"]
        url = "%s?service=%s" % (self._base_url, api_name)
        req = build_request(
            url=url,
            method="p",
            params=data,
            auth=token
        )
        self._response = req.response

        return self._parse_standard_resp(req.json_dict)
