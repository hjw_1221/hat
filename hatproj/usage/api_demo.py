from page.nsp.api.delivery_api import DeliveryApi

base_url = "http://192.168.2.88/nsa/public/"
delivery_api = DeliveryApi(base_url)
resp = delivery_api.login_admin("admin", "123456")

print(resp)

resp = delivery_api.get_by_no("2019041416090001")

print(resp)
order_id = resp["order_id"]

resp1 = delivery_api.get(order_id=order_id)
print(resp1)

order_goods_id_array = resp1["order_goods_id_list"]
express_info = '{"expressCompanyCode":1, "expressName":"哈哈快递3", "expressNo":"k1234567890"}'
resp2 = delivery_api.add(order_id=order_id,
                         express_type=1,
                         express_info=express_info,
                         order_goods_id_array=order_goods_id_array)

print(resp2)
