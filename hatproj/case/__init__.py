__version__ = "2.0.1"
__author__ = "ArtLinty"

import time
import allure
import pytest
from base import BoxDriver, BoxRequest, Csv, Db, Logger, Path, Yaml


class _BaseCase(object):
    _logger = None
    _url = None

    @property
    def test_time(self):
        return time.strftime("%Y%m%d_%H%M%S", time.localtime())

    def log(self, msg):
        """
        添加日志
        :param msg:
        :return:
        """
        if self._logger is not None:
            self._logger.info(msg)

    def assert_in(self, expected, actual):
        """
        assert in
        :param expected:
        :param actual:
        :return:
        """
        result = expected in actual
        log_msg = "断言 assert_in：{%r} in {%r} 的结果是 {%r}！" % (expected, actual, result)

        self.log(log_msg)
        return result

    def assert_equal(self, expected, actual):
        """
        assert equal
        :param expected:
        :param actual:
        :return:
        """
        result = expected == actual
        log_msg = "断言 assert_equal：{%r} == {%r} 的结果是 {%r}！" % (expected, actual, result)

        self.log(log_msg)
        return result


class WebCase(_BaseCase):
    """
    测试用例类

    """
    _driver = None

    @property
    def driver(self):
        return self._driver

    @pytest.fixture(autouse=True)
    def prepare(self):
        self._driver = BoxDriver(BoxDriver.DriverType.CHROME)
        self._logger = Logger(__file__ + "abc.log", __name__)
        self.log("start test case exec!")
        yield
        self.log("finish test case exec!")
        self._driver.quit()

    @allure.step
    def snapshot(self, name):
        """
        snapshot
        :return:
        """
        if self._driver is not None:
            image = self._driver.save_window_snapshot_by_png()
            allure.attach(image, name=name, attachment_type=allure.attachment_type.PNG)

    def assert_in(self, expected, actual):
        """
        assert in
        :param expected:
        :param actual:
        :return:
        """
        result = expected in actual
        log_msg = "断言 assert_in：{%r} in {%r} 的结果是 {%r}！" % (expected, actual, result)
        if not result:
            self.snapshot(log_msg)
        self.log(log_msg)
        return result

    def assert_equal(self, expected, actual):
        """
        assert equal
        :param expected:
        :param actual:
        :return:
        """
        result = expected == actual
        log_msg = "断言 assert_equal：{%r} == {%r} 的结果是 {%r}！" % (expected, actual, result)
        if not result:
            self.snapshot(log_msg)
        self.log(log_msg)
        return result

    def assert_is(self, expected, actual):
        """

        :param expected:
        :param actual:
        :return:
        """
        result = expected is actual
        log_msg = "断言 assert_is：{%r} is {%r} 的结果是 {%r}！" % (expected, actual, result)
        if not result:
            self.snapshot(log_msg)
        self.log(log_msg)
        return result


class ApiCase(_BaseCase):
    """
    API case
    """

    @pytest.fixture(autouse=True)
    def prepare(self):
        self._logger = Logger(__file__ + "abc.log", __name__)
        self.log("start test case exec!")
        yield
        self.log("finish test case exec!")
