import allure
import pytest

from base import Path, Logger
from case import ApiCase
from page.nsp.api.cart_api import CartApi


class TestNspCart(ApiCase):
    _log_file = None

    TEST_CASE_LINK = 'https://xxxx.com/qameta/allure-integrations/issues/8#issuecomment-888888'
    STORY_LINK = 'https://xxxx.com/qameta/allure-integrations/issues/8#issuecomment-666666'

    @pytest.fixture(autouse=True)
    def prepare(self):
        self._url = "http://192.168.2.88/nspapi2/public/"
        log_file = Path.get_actual_path(__file__, "../report/log/%s__%s.log" % (__name__, self.test_time))
        self._logger = Logger(log_file, __name__)
        self.log("setup url")

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story(STORY_LINK, "STORY [101] 成员查看当前自己的购物车功能")
    @allure.testcase(TEST_CASE_LINK, "CASE [996] 成员查看当前自己的购物车功能")
    def test_cart_get_current(self):

        api = CartApi(self._url, logger=self._logger)
        token = api.login_member("liu3", "123456")
        expected = "CtrFFrfLJrAKPzInEyQRXZTB9FNpTnTeeXsTrHTuLV8QGBtAKXMBrQ=="

        assert self.assert_equal(actual=token, expected=expected)

        good_dict = api.get_current()
        assert self.assert_equal(actual=good_dict["items_count"], expected=2)

        expected = "铝框网红ins行李箱拉杆箱旅行箱包密码皮箱子万向轮20寸24男女26"
        assert self.assert_equal(actual=good_dict["69"], expected=expected)

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story(STORY_LINK, "STORY [102] 管理员查看指定成员的购物车商品功能")
    @allure.testcase(TEST_CASE_LINK, "CASE [996] 管理员查看指定成员的购物车商品功能")
    def test_cart_get_list(self):
        log_file = Path.get_actual_path(__file__, "../report/log/%s__%s.log" % (__name__, self.test_time))

        self._logger = Logger(log_file, __name__)
        api = CartApi(self._url, logger=self._logger)
        token = api.login_admin("admin", "123456")
        expected = "ObOTuQv5J5JM/bRsofWXhLFypyQ1SW8QL3ds2niU7RricCSmW7cEtQ=="

        assert self.assert_equal(actual=token, expected=expected)

        good_dict = api.get_list(5)
        assert self.assert_equal(actual=good_dict["items_count"], expected=2)

        expected = "铝框网红ins行李箱拉杆箱旅行箱包密码皮箱子万向轮20寸24男女26"
        assert self.assert_equal(actual=good_dict["69"], expected=expected)
