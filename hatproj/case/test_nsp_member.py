import allure
import pytest

from base import Path, Logger
from case import ApiCase
from page.nsp.api.user_api import UserApi


class TestNspMember(ApiCase):
    _log_file = None

    TEST_CASE_LINK = 'https://xxxx.com/qameta/allure-integrations/issues/8#issuecomment-888888'
    STORY_LINK = 'https://xxxx.com/qameta/allure-integrations/issues/8#issuecomment-666666'

    @pytest.fixture(autouse=True)
    def prepare(self):
        self._url = "http://192.168.2.88/nspapi2/public/"
        # 准备一个 日志文件
        log_file = Path.get_actual_path(__file__, "../report/log/test_nsp_member__%s.log" % self.test_time)
        self._logger = Logger(log_file, __name__)
        self.log("setup url")

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story(STORY_LINK, "STORY [101] 成员登录功能")
    @allure.testcase(TEST_CASE_LINK, "CASE [996] 有效和无效用户登录验证")
    def test_member_login(self):

        # 准备业务，例如 UserApi 类实例化， 需要 url，可选 log
        api = UserApi(self._url, logger=self._logger)
        self.log("UserApi object created! ")

        # 调用 api
        token = api.login_member("liu3", "123456")
        self.log("login_member api called! ")

        expected = "CtrFFrfLJrAKPzInEyQRXZTB9FNpTnTeeXsTrHTuLV8QGBtAKXMBrQ=="

        assert self.assert_equal(actual=token, expected=expected)

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story(STORY_LINK, "STORY [102] 管理员登录功能")
    @allure.testcase(TEST_CASE_LINK, "CASE [996] 有效和无效用户登录验证")
    def test_admin_login(self):
        log_file = Path.get_actual_path(__file__, "../report/log/test_nsp_admin__%s.log" % self.test_time)

        self._logger = Logger(log_file, __name__)
        api = UserApi(self._url, logger=self._logger)
        token = api.login_member("admin", "123456")
        expected = "ObOTuQv5J5JM/bRsofWXhLFypyQ1SW8QL3ds2niU7RricCSmW7cEtQ=="

        assert self.assert_equal(actual=token, expected=expected)
