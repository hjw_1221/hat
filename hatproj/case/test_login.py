from time import sleep

import allure
import pytest

from base import BoxDriver, Csv, Path, Logger, Yaml
from case import WebCase
from page.ranzhi.main_page import MainPage


class TestLogin(WebCase):
    """
    用例 [rzat01] 系统登录模块能接受不同的语言和不同类型的用户在有效和无效范围内登录。
    """

    _log_file = None
    _csv_data = Csv.read_as_dict2(Path.get_actual_path(__file__, "../case/data/test_login__test_user_test.csv"))

    TEST_CASE_LINK = 'https://github.com/qameta/allure-integrations/issues/8#issuecomment-888888'
    STORY_LINK = 'https://github.com/qameta/allure-integrations/issues/8#issuecomment-666666'

    @pytest.fixture(autouse=True)
    def prepare(self):
        """
        测试前置条件
        :return:
        """
        self._driver = BoxDriver(BoxDriver.DriverType.CHROME)
        self._url = "http://pro.demo.ranzhi.org"
        log_file = Path.get_actual_path(__file__, "../report/log/test_login__%s.log" % self.test_time)

        self._logger = Logger(log_file, __name__)
        self.main_page = MainPage(self._driver, self._logger)
        self.log("start test case exec!")
        yield
        self.log("finish test case exec!")
        self._driver.quit()

    @pytest.mark.parametrize("dt", _csv_data)
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story(STORY_LINK, "STORY [101] 登录功能")
    @allure.testcase(TEST_CASE_LINK, "CASE [996] 有效和无效用户登录验证")
    def test_user_login(self, dt):
        """
        测试有效和无效用户登录
        :return:
        """
        print(dt)
        print(dt["用户名"])
        account = "usage"
        password = "usage"

        self.main_page.open(self._url)
        self.snapshot("截图1")

        self.main_page.change_language("zh_TW")
        self.log("切换到繁体中文")

        # 这里对当前步骤进行截图
        # self.images.append(self._driver.save_window_snapshot_by_io())
        self.snapshot("截图2")

        # 添加日志
        self.log("刚进行了新的日志编写，切换简体中文")
        lang = "zh_CN"
        self.main_page.change_language(lang)
        self.snapshot("截图3")

        # 检查语言
        actual_text_dict = self.main_page.text_dict_for_language
        assert self.assert_equal("简体", actual_text_dict["lang"]), "切换语言后，语言按钮文字不匹配！"
        assert self.assert_equal("登录", actual_text_dict["login"]), "切换语言后，语言按钮文字不匹配！"

        self.main_page.login_and_submit(account, password, True)
        self.snapshot("截图4")

        sleep(2)

        # 根据是否有效等价检查登录

        expected_url = self._url + "/sys/index.php?m=index&f=index"
        actual_url = self._driver.get_url()
        assert self.assert_equal(expected_url, actual_url), "有效登录用户[%s]登录失败或者登录后跳转错误！" % account

        self.main_page.do_logout_by_lang(lang)
        self.snapshot("截图5")
        # 检查登出
        expected_url = self._url + "/sys/index.php?m=user&f=login"
        actual_url = self._driver.get_url()
        assert self.assert_equal(expected_url, actual_url), "有效登录用户[%s]退出失败或者退出后跳转错误！" % account

    @allure.severity(allure.severity_level.NORMAL)
    @allure.story(STORY_LINK, "STORY [102] 遍历应用切换")
    @allure.testcase(TEST_CASE_LINK, "CASE [997] 所有 app 都可以切换")
    def test_app_select(self):
        """
        测试选择 app
        :return:
        """
        self.log("[RanzhiTest]开始 test_02")
        self.main_page.open(self._url)
        self.main_page.input_login_info("usage", "usage")

        self.main_page.do_login()

        self.main_page.select_app("oa")
        self.snapshot("oa 截图")
        self.main_page.select_app("crm")
        self.main_page.select_app("cash")
        self.main_page.select_app("proj")
        self.log("[RanzhiTest]结束 test_02")
