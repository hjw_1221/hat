import threading

from base import build_driver, build_request, start_mock


def test_chrome():
    chrome = build_driver("http://www.baidu.com", 'c')
    chrome.quit()


def test_firefox():
    firefox = build_driver("http://www.baidu.com", 'f')
    firefox.quit()


def test_get():
    req = build_request("http://localhost:5678/mocks", 'g')
    print(req.json_string)


def test_get2():
    req = build_request("http://localhost:5678/mock/102", 'g')
    print(req.json_string)


def test_post():
    payload = {'mock': 'Aobama'}
    req = build_request("http://localhost:5678/mocks", method="POST", params=payload)
    print(req.json_string)


def test_put():
    payload = {'mock': 'JackMa'}
    req = build_request("http://localhost:5678/mock/%r" % 102, method="PUT", params=payload)
    print(req.json_string)


def test_delete():
    req = build_request("http://localhost:5678/mock/%r" % 103, method="DELETE")
    print(req.json_string)


def test_ts_post():
    token_key = "46c575222b62960bf8699eceae154d7c0239bf30ebda07be94901ec0"
    api_url = "http://api.tushare.pro"
    stock_code = '601789.SH'
    api_name = "daily"
    args = {
        "ts_code": stock_code,
        "start_date": '20190101',
        "end_date": '20190308'
    }

    req_params = {
        'api_name': api_name,
        'token': token_key,
        'params': args,
        'fields': ""
    }

    # res 是请求返回来的响应数据，以下是 data_api.query() 里面的源代码
    req = build_request(api_url, method="POST", params=req_params)

    print(req.json_string)
    print(req.status_code)
    print(req.response_headers)
