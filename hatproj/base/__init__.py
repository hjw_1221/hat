__version__ = "2.0.1"
__author__ = "ArtLinty"

# Licensed to the Software Freedom Conservancy (SFC) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The SFC licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

# Usage：
# call build_driver() for web browser instance
# call build_request() for web api instance

from base.box import _BoxDriver
from base.box import _BoxRequest
from base.helper import _CsvHelper, _PathHelper, _DbHelper, _YamlHelper, _JsonHelper
from base.infra import _Logger

"""
main portal, for web browser driver
"""


def build_driver(url, driver_type):
    """
    get driver for web browser testing
    :param url: str，访问浏览器的 url
    :param driver_type: str，指定浏览器的类型，忽略大小写
        : c, chrome
        : i, ie
        : f, firefox
        : o, opera
        : s, safari
        : h, headless, headless_chrome, hc
    :return: BoxDriver
    """
    if not isinstance(driver_type, str):
        driver_type = 'c'

    driver_type = _parse_type(driver_type.lower())
    driver = BoxDriver(driver_type)
    driver.navigate(url)
    driver.maximize_window()
    driver.forced_wait(5)
    return driver


"""
main portal, for web api request
"""


def build_request(url,
                  method,
                  headers=None,
                  cookies=None,
                  auth=None,
                  params=None):
    """
    get request for web api testing
    :param url: full api request url
    :param method: request method, str, 忽略大小写
    :param headers: request headers，dict
    :param cookies: request cookies， dict
    :param auth: request authorization， tuple，dict or str
    :param params: request params，dict， for all request methods
    :return: BoxRequest
    """

    # method type: str, or default p
    if not isinstance(method, str):
        method = 'p'
    return BoxRequest(url=url,
                      method=_parse_method(method).upper(),
                      headers=headers,
                      cookies=cookies,
                      auth=auth,
                      params=params)


"""
private method
"""


def _parse_type(driver_type):
    """
    parse driver type
    :param driver_type: str: driver type
        chrome: c or chrome
        firefox: f or firefox
        ie: i or ie
        safari: s or safari
        headless chrome: h or hc or headless, headless chrome
    :return: default value: BoxDriver.DriverType.CHROME
    """
    if driver_type == 'c' or driver_type == "chrome":
        return BoxDriver.DriverType.CHROME
    elif driver_type == 'f' or driver_type == "firefox":
        return BoxDriver.DriverType.FIREFOX
    elif driver_type == 'i' or driver_type == "ie":
        return BoxDriver.DriverType.IE
    elif driver_type == 's' or driver_type == "safari":
        return BoxDriver.DriverType.SAFARI
    elif driver_type == 'h' or driver_type == "headless" \
            or driver_type == "hc" or driver_type == "headless_chrome":
        return BoxDriver.DriverType.CHROME_HEADLESS

    return BoxDriver.DriverType.CHROME


def _parse_method(method):
    """
    parse request method
    :param method: str request method
        get: g or get
        post: p or post
        delete: d or delete
        put: u or put
        head: h or head
    :return: default value: POST
    """
    if method == 'g' or method == "get":
        return "GET"
    elif method == 'p' or method == "post":
        return "POST"
    elif method == 'd' or method == "delete":
        return "DELETE"
    elif method == 'u' or method == "put":
        return "PUT"
    elif method == 'h' or method == "head":
        return "HEAD"

    return "POST"


"""
public classes
"""


class BoxDriver(_BoxDriver):
    """
    BoxDriver
    """
    pass


class BoxRequest(_BoxRequest):
    """
    BoxRequest
    """
    pass


class Csv(_CsvHelper):
    """
    Csv
    """
    pass


class Db(_DbHelper):
    """
    Db
    """
    pass


class Path(_PathHelper):
    """
    Path
    """
    pass


class Yaml(_YamlHelper):
    """
    Yaml
    """
    pass


class Logger(_Logger):
    """
    Logger
    """
    pass


class Json(_JsonHelper):
    pass
